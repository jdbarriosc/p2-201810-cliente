package model.vo;

public class RangoF_H implements Comparable<RangoF_H>
{
	private int rangoSuperior;
	private int rangoInferior;
	private String fechaIni;

	public RangoF_H(String fechaIni,String horaInicial)
	{
		this.fechaIni=fechaIni;
		int rangoInf=0;

		String tiem[]=horaInicial.split(":");

		int hora=Integer.parseInt(tiem[0]);
		int min=Integer.parseInt(tiem[1]);


		boolean encontro=false;

		while(!encontro)
		{

			if(min>=rangoInf&&min<(rangoInf+15))
			{

				rangoInferior=hora*100+rangoInf;
				rangoSuperior=hora*100+rangoInf+14;
				encontro=true;

			}
			rangoInf+=15;
		}


	}



	public int getRangoSuperior() {
		return rangoSuperior;
	}

	public void setRangoSuperior(int rangoSuperior) {
		this.rangoSuperior = rangoSuperior;
	}

	public int getRangoInferior() {
		return rangoInferior;
	}

	public void setRangoInferior(int rangoInferior) {
		this.rangoInferior = rangoInferior;
	}
	public String getFechaIni()
	{
		return fechaIni;
	}

	@Override
	public int compareTo(RangoF_H arg0) {
		if(fechaIni.equals(arg0.getFechaIni()))
			return rangoInferior-arg0.getRangoInferior();
		else
			return fechaIni.compareTo(arg0.getFechaIni());
	}


}
