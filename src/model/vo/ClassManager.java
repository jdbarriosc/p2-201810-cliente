package model.vo;

import java.util.Comparator;
import java.util.Iterator;

import model.data_structures.HeapSort;
import model.data_structures.ILinkedList;
import model.data_structures.IQueue;
import model.data_structures.IStack;
import model.data_structures.LinearProbingHashST;
import model.data_structures.LinkedListQueue;
import model.data_structures.LinkedListStack;
import model.data_structures.ListaDoblementeEncadenada;
import model.data_structures.PriorityQueueListaDoble;
import model.data_structures.RedBlackBST;
import model.data_structures.RedBlackBSTList;
import model.data_structures.SeparateChainingHashST;
import model.vo.Taxi.Comparador;

public class ClassManager  {

	private ListaDoblementeEncadenada<Servicio> servicios;
	private ListaDoblementeEncadenada<Servicio> serviciosAux;
	private ListaDoblementeEncadenada<Taxi> taxis;
	private ListaDoblementeEncadenada<Compania> companias;
	private RedBlackBST<String, ListaDoblementeEncadenada<Taxi>> arbolCompanias;
	private SeparateChainingHashST<Integer, Servicio> tablaPorTiempo;
	private RedBlackBST<RangoF_H, ListaDoblementeEncadenada<Servicio>> arbolServRGFH;
	private RedBlackBST<Double, ListaDoblementeEncadenada<Servicio>> arbolServDistancias;
	private LinearProbingHashST<String, RedBlackBSTList<RangoFechaHora,Servicio>> tablaArbolesServiciosPorZonas;
	private LinearProbingHashST<Integer, RedBlackBSTList<String, Servicio>> tablaDistanciaHarvesiana;
	private double[] promedioLatLong;

	public ClassManager()
	{
		servicios=new ListaDoblementeEncadenada<Servicio>();
		taxis=new ListaDoblementeEncadenada<Taxi>();
		companias=new ListaDoblementeEncadenada<Compania>();
		serviciosAux=new ListaDoblementeEncadenada<Servicio>();
		arbolCompanias=new RedBlackBST<>();
		arbolServDistancias=new RedBlackBST<>();
		arbolServRGFH=new RedBlackBST<>();
		promedioLatLong=new double[3];

	}
	public ListaDoblementeEncadenada<Servicio> getServicios() {
		return servicios;
	}
	public void setServicios(ListaDoblementeEncadenada<Servicio> servicios) {
		this.servicios = servicios;
	}
	public ListaDoblementeEncadenada<Servicio> getServiciosAux() {
		return serviciosAux;
	}
	public void setServiciosAux(ListaDoblementeEncadenada<Servicio> serviciosAux) {
		this.serviciosAux = serviciosAux;
	}
	public ListaDoblementeEncadenada<Taxi> getTaxis() {
		return taxis;
	}
	public void setTaxis(ListaDoblementeEncadenada<Taxi> taxis) {
		this.taxis = taxis;
	}
	public ListaDoblementeEncadenada<Compania> getCompanias() {
		return companias;
	}
	public void setCompanias(ListaDoblementeEncadenada<Compania> companias) {
		this.companias = companias;
	}

	public void agregarTaxiServicioACompania(String nombre, Taxi taxi, Servicio servicio)
	{


		boolean esta=false;
		Iterator<Compania> iter=companias.iterator();
		while(iter.hasNext()&&!esta)
		{
			Compania actual=iter.next();
			if(actual.getNombre().equalsIgnoreCase(nombre))
			{
				esta=true;
				actual.addTaxi(taxi);
				actual.addServicio(servicio);
			}
		}


		if(!esta)
			companias.add(new Compania(nombre,taxi,servicio));


	}
	public Taxi agregarTaxi(String id, String compania, Servicio serv)
	{

		boolean esta=false;
		Iterator<Taxi> iter=taxis.iterator();
		while(iter.hasNext()&&!esta)
		{
			Taxi actual=iter.next();
			if(actual.getTaxiId().equalsIgnoreCase(id))
			{
				esta=true;
				actual.addSevice(serv);
			}
		}


		if(!esta)
		{
			Taxi taxi=new Taxi(id,compania,serv);
			taxis.add(taxi);
			return taxi;
		}
		return null;

	}




	public void agregarServicio(int estado,String tripId, String taxiId, String seconds, String miles, String total,String fechaInicial, String horaInicial, String fechaFinal, String horaFinal, String compania,String zonaInicio, String zonaFinal,String pickupLatitud, String pickupLongitud)
	{
		if(estado==0)
		{
			servicios.enlazar(serviciosAux);
			serviciosAux.limpiar();
			return;
		}

		if(estado==1)
		{
			servicios.addInOrder(new Servicio(compania,tripId, taxiId, seconds,miles, total, fechaInicial, horaInicial,fechaFinal,horaFinal,zonaInicio,zonaFinal, pickupLatitud!=null?Double.parseDouble(pickupLatitud):Double.NaN, pickupLongitud!=null?Double.parseDouble(pickupLongitud):Double.NaN));
			if(pickupLatitud!=null&&pickupLongitud!=null)
			{
				promedioLatLong[0]+=Double.parseDouble(pickupLatitud);
				promedioLatLong[1]+=Double.parseDouble(pickupLongitud);
				promedioLatLong[2]++;
			}

		}
		else if(estado>1)
		{
			serviciosAux.addInOrder(new Servicio(compania,tripId, taxiId, seconds,miles, total, fechaInicial, horaInicial,fechaFinal,horaFinal,zonaInicio,zonaFinal, pickupLatitud!=null?Double.parseDouble(pickupLatitud):Double.NaN, pickupLongitud!=null?Double.parseDouble(pickupLongitud):Double.NaN));
			if(pickupLatitud!=null&&pickupLongitud!=null)
			{
				promedioLatLong[0]+=Double.parseDouble(pickupLatitud);
				promedioLatLong[1]+=Double.parseDouble(pickupLongitud);
				promedioLatLong[2]++;
			}



		}

	}

	public void acomodarPromedio()
	{
		promedioLatLong[0]/=promedioLatLong[2];
		promedioLatLong[1]/=promedioLatLong[2];
		promedioLatLong[2]=1;


	}

	public void inicializarTablasListasArboles()
	{
		SeparateChainingHashST<Double, Servicio> tablaPorDistancia=new SeparateChainingHashST<Double, Servicio>(servicios.size());
		SeparateChainingHashST<String, Servicio> tablaPorZonaIF=new SeparateChainingHashST<String, Servicio>(servicios.size());
		tablaPorTiempo=new SeparateChainingHashST<Integer, Servicio>(servicios.size());
		SeparateChainingHashST<Integer, Servicio> tablaHarvesiana=new SeparateChainingHashST<Integer, Servicio>(servicios.size());
		Iterator<Servicio> iter= servicios.iterator();
		while(iter.hasNext())
		{
			Servicio actual=iter.next();
			if(!actual.getTripMiles().equals("0"))
				try 
			{
					tablaPorDistancia.put(Double.parseDouble(actual.getTripMiles()), actual);
			}
			catch(Exception e)
			{
				System.out.println("Hay millas que nos se pueden parsear");
			}


			if(!actual.getTripSeconds().equals("0"))
				try
			{
					int seg=Integer.parseInt(actual.getTripSeconds());
					seg=seg%60==0?seg/60:(seg/60)+1;
					
					tablaPorTiempo.put(seg, actual);
			}
			catch(Exception e)
			{
				System.out.println("Hay segundos que nos se pueden parsear");
			}
			if(!(actual.getPickupLatitud()+"").equals("NaN")&&!(actual.getPickupLongitud()+"").equals("NaN"))
				try
			{
					String dist=""+actual.getDistanceInMiles(promedioLatLong[0], promedioLatLong[1]);
					int mult=Integer.parseInt(dist.substring(0, dist.indexOf(".")));
					int grupo=Integer.parseInt(dist.substring(dist.indexOf(".")+1,dist.indexOf(".")+2))+1+(10*mult);
					tablaHarvesiana.put(grupo, actual);		

			}
			catch(Exception e)
			{
				System.out.println("Hay distancias que no se pueden parsear");
			}


			if(!actual.getZonaInicio().equals("-1")&&!actual.getZonaFinal().equals("-1"))
				tablaPorZonaIF.put(actual.getZonaInicio()+"-"+actual.getZonaFinal(), actual);
			Taxi taxi= agregarTaxi(actual.getTaxiId(), actual.getCompania(), actual);
			if(taxi!=null)	
				agregarTaxiServicioACompania(actual.getCompania(), taxi, actual);

		}	
		llenarArbolServiciosDistancias(tablaPorDistancia);
		inicializarTablaArbolesServiciosZonas(tablaPorZonaIF);
		inicializarTablaArbolesServiciosDistanciaHarvesiana(tablaHarvesiana);
		llenarArbolCompanias();
	}


	public void llenarArbolCompanias()
	{
		for(Compania actual:companias)
		{
			for (Taxi actualT: actual.getTaxisInscritos()) 
			{
				actualT.inicializarTablaHashUbi();
			}
			arbolCompanias.put(actual.getNombre(), actual.getTaxisInscritos());

		}
	}

	public void llenarArbolServiciosDistancias(SeparateChainingHashST<Double, Servicio> tablaPorDistancia)
	{
		Iterator<Double> iter= tablaPorDistancia.keys().iterator();
		while(iter.hasNext())
		{
			Double key=iter.next();
			arbolServDistancias.put(key, tablaPorDistancia.get(key));
		}
	}

	public void inicializarTablaArbolesServiciosZonas(SeparateChainingHashST<String, Servicio> tablaPorZonaIF)
	{
		Iterator<String> iter= tablaPorZonaIF.keys().iterator();
		tablaArbolesServiciosPorZonas=new LinearProbingHashST<String,RedBlackBSTList<RangoFechaHora,Servicio>>(tablaPorZonaIF.sizeKeys());
		while(iter.hasNext())
		{
			String key=iter.next();
			RedBlackBSTList<RangoFechaHora, Servicio> arbol=new RedBlackBSTList<RangoFechaHora, Servicio>();
			for(Servicio serv:tablaPorZonaIF.get(key))
			{
				arbol.put(serv.getFecha(), serv);
			}
			tablaArbolesServiciosPorZonas.put(key, arbol);
		}
	}

	public void inicializarTablaArbolesServiciosDistanciaHarvesiana(SeparateChainingHashST<Integer, Servicio> tablaHarvesiana)
	{

		Iterator<Integer> iter= tablaHarvesiana.keys().iterator();
		tablaDistanciaHarvesiana=new LinearProbingHashST<Integer,RedBlackBSTList<String,Servicio>>(tablaHarvesiana.sizeKeys());
		while(iter.hasNext())
		{
			Integer key=iter.next();
			RedBlackBSTList<String, Servicio> arbol=new RedBlackBSTList<String, Servicio>();
			for(Servicio serv:tablaHarvesiana.get(key))
			{
				arbol.put(serv.getTaxiId(), serv);
			}
			tablaDistanciaHarvesiana.put(key, arbol);;
		}

	}

	public void inicializarArbolServRGFH()
	{
		SeparateChainingHashST<RangoF_H, Servicio>tablaAux= new SeparateChainingHashST<>(servicios.size());

		Iterator<Servicio> iter=servicios.iterator();

		while (iter.hasNext())
		{
			Servicio actual=iter.next();
			tablaAux.put(actual.getRangoF_H(), actual);
		}
		Iterator<RangoF_H>iter2=tablaAux.keys().iterator();
		while(iter2.hasNext())
		{
			RangoF_H key=iter2.next();
			ListaDoblementeEncadenada<Servicio>serviciosActual=darListaServiciosRGFecha(key);

			arbolServRGFH.put(key, serviciosActual);
		}
	}

	public ListaDoblementeEncadenada<Servicio> darListaServiciosRGFecha(RangoF_H fh)
	{
		ListaDoblementeEncadenada<Servicio>serviciosRGFecha=new ListaDoblementeEncadenada<>();
		for(Servicio actual:servicios)
		{
			if(actual.getRangoF_H().compareTo(fh)==0)
			{
				serviciosRGFecha.add(actual);

			}
		}
		return serviciosRGFecha;
	}





	/**
	 * --------------------------------------------------------------------------
	 * PROYECTO 2
	 * --------------------------------------------------------------------------
	 */

	//1A
	public ListaDoblementeEncadenada<TaxiEnZona> A1TaxiConMasServiciosEnZonaParaCompania(String idZona, String nombreCompania )
	{
		TaxiEnZona mayor=new TaxiEnZona();

		mayor.setCompany(nombreCompania);
		mayor.setZona(idZona);
		int numMayor=0;
		ListaDoblementeEncadenada<TaxiEnZona> taxiMasServ=new ListaDoblementeEncadenada<>();
		Iterator<Taxi>iter=arbolCompanias.get(nombreCompania).iterator();
		while(iter.hasNext())
		{
			Taxi actual=iter.next();
			if(actual.darServiciosZona(idZona)!=null)
			{
				if(actual.darServiciosZona(idZona).size()>numMayor)
				{
					mayor.setIdTaxi(actual.getTaxiId());
					mayor.setServiciosPrestadosEnRango(actual.darServiciosZona(idZona));
					taxiMasServ=new ListaDoblementeEncadenada<>();
					taxiMasServ.add(mayor);
					numMayor=actual.darServiciosZona(idZona).size();
				}

				else if(actual.darServiciosZona(idZona).size()==numMayor)
				{

					TaxiEnZona mayor2=new TaxiEnZona();
					mayor2.setCompany(nombreCompania);
					mayor2.setZona(idZona);
					mayor2.setIdTaxi(actual.getTaxiId());
					mayor2.setServiciosPrestadosEnRango(actual.darServiciosZona(idZona));
					taxiMasServ.add(mayor2);

				}
			}
		}
		return taxiMasServ;

	}

	//2A
	public ListaDoblementeEncadenada<Servicio> A2ServiciosPorDuracion(int duracion)
	{

		duracion=duracion%60==0?duracion/60:(duracion/60)+1;
System.out.println(tablaPorTiempo.get(duracion).size());
		return tablaPorTiempo.get(duracion);

	}

	//1B
	public ListaDoblementeEncadenada<Servicio> B1ServiciosPorDistancia(double distMin, double distMax)
	{
		ListaDoblementeEncadenada<Servicio> lista=new ListaDoblementeEncadenada<Servicio>();
		for(ListaDoblementeEncadenada<Servicio> list:arbolServDistancias.getBetween(distMin,distMax))
			lista.enlazar(list);
		return lista;

	}

	//2B
	public ListaDoblementeEncadenada<Servicio> B2ServiciosPorZonaRecogidaYLlegada(String inicioFin, String fechaI, String fechaF, String horaI, String horaF ) 
	{
		RedBlackBSTList<RangoFechaHora, Servicio> arbol=tablaArbolesServiciosPorZonas.get(inicioFin);
		ListaDoblementeEncadenada<Servicio>lista=new ListaDoblementeEncadenada<Servicio>();
		if(arbol!=null)
			for(ListaDoblementeEncadenada<Servicio> list:arbol.getBetween(new RangoFechaHora(fechaI, null, horaI, null), new RangoFechaHora(fechaF, null, horaF, null))) 
			{
				lista.enlazar(list);
			}
		else
			return null;
		return lista;
	}





	//1C
	public TaxiConPuntos[] R1C_OrdenarTaxisPorPuntos()
	{
		TaxiConPuntos[]tax=new TaxiConPuntos[taxis.size()];
		int contador=0;
		for(Taxi actual:taxis)
		{

			tax[contador]=new TaxiConPuntos(actual.getTaxiId(), actual.getCompany(), actual.getPuntos());
			contador++;
		}
		HeapSort<TaxiConPuntos>heap=new HeapSort<TaxiConPuntos>();
		tax=heap.sort(tax);

		//TODO 
		return tax;


	}

	//2C
	public ListaDoblementeEncadenada<Servicio> R2C_LocalizacionesGeograficas(String taxiIDReq2C, double millasReq2C) {
		// TODO Auto-generated method stub
		String dist=millasReq2C+"";
		int mult=Integer.parseInt(dist.substring(0, dist.indexOf(".")));
		RedBlackBSTList<String,Servicio> arbol=tablaDistanciaHarvesiana.get(Integer.parseInt(dist.substring(dist.indexOf(".")+1,dist.indexOf(".")+2))+1+(10*mult));
		return arbol!=null?arbol.get(taxiIDReq2C):null;

	}

	//3C
	public ListaDoblementeEncadenada<Servicio> R3C_ServiciosEn15Minutos(String fecha,String hora,String zonaIni, String zonaFin)
	{
		RangoF_H rg=new RangoF_H(fecha, hora);
		ListaDoblementeEncadenada<Servicio>serv= arbolServRGFH.get(rg);
		ListaDoblementeEncadenada<Servicio>retorno=new ListaDoblementeEncadenada<>();
		for(Servicio actual:serv)
		{
			if(actual.getZonaInicio().equals(zonaIni)&&actual.getZonaFinal().equals(zonaFin))
			{
				retorno.add(actual);
			}
		}
		return retorno;
	}




}
