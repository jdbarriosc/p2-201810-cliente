package model.vo;

/**
 * Representation of a Service object
 */

public class Servicio implements Comparable<Servicio>
{	
	private String taxiId;
	private String tripId;
	private String tripSeconds;
	private String tripMiles;
	private String tripTotal;
	private RangoFechaHora fecha;
	private String zonaInicio;
	private String zonaFinal;
	private String compania;
	private double pickupLatitud;
	private double pickupLongitud;
	private RangoF_H rangoF_H;
	private double distanciaAReferencia;
private RangoDistancia rgDistancia;

	public Servicio(String compania,String tripId, String taxiId, String seconds, String miles, String total,String fechaInicial, String horaInicial, String fechaFinal, String horaFinal, String zonaI, String zonaF,double pickupLatitud, double pickupLongitud)
	{

		this.tripId=tripId;
		this.taxiId=taxiId;
		tripSeconds=seconds;
		tripMiles=miles;
		tripTotal=total;
		fecha= new RangoFechaHora(fechaInicial,fechaFinal,horaInicial,horaFinal);
		zonaInicio=zonaI;
		zonaFinal=zonaF;
		this.compania=compania;
		this.pickupLatitud=pickupLatitud;
		this.pickupLongitud=pickupLongitud;
		setRangoF_H(new RangoF_H(fechaInicial, horaInicial));
	}

	
	
	 public double getPickupLatitud(){
		
	     return pickupLatitud;
	      
     }

     public double getPickupLongitud(){
         
         return pickupLongitud;
       
     }
	
	public String getCompania() {
		return compania;
	}


	public void setCompania(String compania) {
		this.compania = compania;
	}

	public String getZonaInicio() {
		return zonaInicio;
	}


	public void setZonaInicio(String zonaInicio) {
		this.zonaInicio = zonaInicio;
	}


	public String getZonaFinal() {
		return zonaFinal;
	}


	public void setZonaFinal(String zonaFinal) {
		this.zonaFinal = zonaFinal;
	}



	public RangoFechaHora getFecha() {
		return fecha;
	}

	public boolean estaEnRango(RangoFechaHora rango)
	{
		return fecha.estaEnRango(rango);
	}

	public boolean iniciaEnRango(RangoFechaHora rango)
	{
		return fecha.iniciaEnRango(rango);
	}
	public boolean terminaEnRango(RangoFechaHora rango)
	{
		return fecha.terminaEnRango(rango);
	}

	public void setFecha(RangoFechaHora fecha) {
		this.fecha = fecha;
	}



	public void setTripId(String tripId) {
		this.tripId = tripId;
	}



	public void setTaxiId(String taxiId) {
		this.taxiId = taxiId;
	}



	public void setTripSeconds(String tripSeconds) {
		this.tripSeconds = tripSeconds;
	}



	public void setTripMiles(String tripMiles) {
		this.tripMiles = tripMiles;
	}



	public void setTripTotal(String tripTotal) {
		this.tripTotal = tripTotal;
	}

	public String getTripId() {
		return tripId;
	}



	public String getTaxiId() {
		return taxiId;
	}



	public String getTripSeconds() {
		return tripSeconds;
	}



	public String getTripMiles() {
		return tripMiles;
	}



	public String getTripTotal() {
		return tripTotal;
	}

	public double getDistanceInMiles(double pLat, double pLon) 
	{
		try
		{
			int r=6371*1000;
			Double latDist=toRad(pickupLatitud-pLat);
			Double lonDist=toRad(pickupLongitud-pLon);
			Double a = Math.sin(latDist/2)*Math.sin(latDist/2)
					+Math.cos(toRad(pLat))*Math.cos(toRad(pickupLatitud))
					*Math.sin(lonDist/2)*Math.sin(lonDist/2);
			Double c=2*Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
			distanciaAReferencia= r*c*0.000621371;
			return distanciaAReferencia;

		}
		catch(Exception e)
		{ 	
			e.printStackTrace();
			System.out.println("entro");
			return Double.NaN;
		}
	}
	
	public double getDistanciaAReferencia()
	{
		return distanciaAReferencia;
	}

	public double toRad(double grados)
	{
		return (grados/90)*3.1416;
	}




	@Override
	public int compareTo(Servicio nServicio) {

		if(fecha.getFechaInicial().compareTo(nServicio.getFecha().getFechaInicial())==0)
			return fecha.getHoraInicio().compareTo(nServicio.getFecha().getHoraInicio());
		return fecha.getFechaInicial().compareTo(nServicio.getFecha().getFechaInicial());	}

	public RangoF_H getRangoF_H() {
		return rangoF_H;
	}

	public void setRangoF_H(RangoF_H rangoF_H) {
		this.rangoF_H = rangoF_H;
	}
}
