package model.vo;

import java.util.Iterator;

import model.data_structures.LinearProbingHashST;
import model.data_structures.ListaDoblementeEncadenada;
import model.data_structures.SeparateChainingHashST;

/**
 * Representation of a taxi object
 */
public class Taxi implements Comparable<Taxi>
{
	public enum Comparador
	{
		PUNTOS,COMPANIA;
	}

	private String taxiId;
	private String compania;
	private ListaDoblementeEncadenada<Servicio> serviciosTaxi ;
	private double dineroTotal=0;
	private double millasTotales=0;
	private SeparateChainingHashST<String, Servicio> tablaPorUbi;
	private double puntos;
	private Comparador comparador;

	public Taxi(String id, String compania,Servicio serv)
	{
		taxiId=id;
		this.compania=compania;
		puntos=0;
		comparador=Comparador.COMPANIA;
		serviciosTaxi=new ListaDoblementeEncadenada<Servicio>(serv);
		dineroTotal+=Double.parseDouble(serv.getTripTotal());
		millasTotales+=Double.parseDouble(serv.getTripMiles());
		if(dineroTotal!=0&&millasTotales!=0)
		{	
			puntos=(dineroTotal+millasTotales)/(dineroTotal/millasTotales)*serviciosTaxi.size();
		}
	}
	public Taxi(String id, String compania)
	{
		taxiId=id;
		this.compania=compania;
		serviciosTaxi=new ListaDoblementeEncadenada<Servicio>();
	}

	public void addSevice(Servicio serv)
	{
		double dinero=Double.parseDouble(serv.getTripTotal());
		double millas=Double.parseDouble(serv.getTripMiles());
		if(dinero!=0&&millas!=0)
		{
			dineroTotal+=dinero;
			millasTotales+=millas;
			puntos=(dineroTotal+millasTotales)/(dineroTotal/millasTotales)*serviciosTaxi.size();
		}
		serviciosTaxi.add(serv);
	}

	public ListaDoblementeEncadenada<Servicio> getServiciosTaxi() {
		return serviciosTaxi;
	}


	/**
	 * @return id - taxi_id
	 */
	public String getTaxiId()
	{
		// TODO Auto-generated method stub
		return taxiId;
	}

	/**
	 * @return company
	 */
	public String getCompany()
	{
		// TODO Auto-generated method stub
		return compania;
	}


	public double darGananciaEnRango(RangoFechaHora rango)
	{
		Iterator<Servicio> iter=serviciosTaxi.iterator();
		double ganancia=0.0;
		while(iter.hasNext())
		{
			Servicio actual=iter.next();
			if(actual.iniciaEnRango(rango))
			{
				if(actual.terminaEnRango(rango))
					ganancia+=Double.parseDouble(actual.getTripTotal());
				while(iter.hasNext()&&(actual=iter.next()).iniciaEnRango(rango))
				{
					if(actual.terminaEnRango(rango))
						ganancia+=Double.parseDouble(actual.getTripTotal());							
				}
				return ganancia;
			}
		}
		return ganancia;


	}
	public int darServEnRango(RangoFechaHora rango)
	{
		Iterator<Servicio> iter=serviciosTaxi.iterator();
		int servicios=0;
		while(iter.hasNext())
		{
			Servicio actual=iter.next();
			if(actual.getFecha().compareTo(rango)==1)
			{
				servicios++;

			}
			else if(actual.getFecha().compareTo(rango)==-1)
			{
				break;
			}
		}
		return servicios;


	}

	
	public double getMillas() {
		return millasTotales;
	}

	public double getDinero() {
		return dineroTotal;
	}

	public void inicializarTablaHashUbi()
	{
		tablaPorUbi= new SeparateChainingHashST<String, Servicio>(serviciosTaxi.size());

		Iterator<Servicio> iter=serviciosTaxi.iterator();

		while (iter.hasNext())
		{
			Servicio actual=iter.next();
			tablaPorUbi.put(actual.getZonaInicio(), actual);
		}
		
		
		

	}
	public ListaDoblementeEncadenada<Servicio> darListaServiciosZona(String idZona)
	{
		ListaDoblementeEncadenada<Servicio>serviciosZona=new ListaDoblementeEncadenada<>();
		for(Servicio actual:serviciosTaxi)
		{
			if(actual.getZonaInicio().equals(idZona))
			{
				serviciosZona.add(actual);

			}
		}
		return serviciosZona;
	}
	public ListaDoblementeEncadenada<Servicio> darServiciosZona(String idZona)
	{
		return tablaPorUbi.get(idZona);
	}
	public double getPuntos() {
		return puntos;
	}

	public void setComparador(Comparador comparador) {
		this.comparador = comparador;
	}
	@Override
	public int compareTo(Taxi o) 
	{
		// TODO Auto-generated method stub
		if(comparador==Comparador.COMPANIA)
			return compania.compareTo(o.compania);
		
		else
		{
			if(puntos-o.getPuntos()>0)
				return 1;
			
			else if(puntos-o.getPuntos()<0)
				return -1;
			else 
				return 0;
		}
	}	


}