package model.vo;

import model.data_structures.ListaDoblementeEncadenada;

public class TaxiEnZona implements Comparable<TaxiEnZona>
{
	//ATRIBUTOS

	/**
	 * Modela el id del taxi 
	 */
	private String idTaxi;


	private String zona;

	/**
	 * modela el nombre de la compa�ia del taxi
	 */
	private String company;


	/**
	 * Modela la lista de servicios que presto el taxi en el rango
	 */
	private ListaDoblementeEncadenada<Servicio> serviciosPrestadosEnZona; 


	//M�TODOS

	/**
	 * @return the idTaxi
	 */
	public String getIdTaxi()
	{
		return idTaxi;
	}

	/**
	 * @param idTaxi the idTaxi to set
	 */
	public void setIdTaxi(String idTaxi)
	{
		this.idTaxi = idTaxi;
	}

	/**
	 * @return the rango
	 */
	public String getZona()
	{
		return zona;
	}

	/**
	 * @param rango the rango to set
	 */
	public void setZona(String zona)
	{
		this.zona = zona;
	}

	/**
	 * @return the company
	 */
	public String getCompany()
	{
		return company;
	}

	/**
	 * @param company the company to set
	 */
	public void setCompany(String company) 
	{
		this.company = company;
	}


	/**
	 * @return the serviciosPrestadosEnRango
	 */
	public ListaDoblementeEncadenada<Servicio> getServiciosPrestadosEnZona()
	{
		return serviciosPrestadosEnZona;
	}

	/**
	 * @param serviciosPrestadosEnRango the serviciosPrestadosEnRango to set
	 */
	public void setServiciosPrestadosEnRango(ListaDoblementeEncadenada<Servicio> serviciosPrestadosEnZona)
	{
		this.serviciosPrestadosEnZona = serviciosPrestadosEnZona;
	}

	/**
	 * @return the distanciaTotalRecorrida
	 */

	@Override
	public int compareTo(TaxiEnZona o) {
		// TODO Auto-generated method stub
		return zona.compareTo(o.getZona());
	}


}
