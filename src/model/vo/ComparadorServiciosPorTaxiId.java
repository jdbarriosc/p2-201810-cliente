package model.vo;

import java.util.Comparator;

public class ComparadorServiciosPorTaxiId  implements Comparator<Servicio> {

		@Override
		public int compare(Servicio serv1, Servicio serv2) {
			if(serv1.getTaxiId().compareTo(serv2.getTaxiId())<0)
				return -1;
			if(serv1.getTaxiId().compareTo(serv2.getTaxiId())>0)
				return 1;
			return 0;
			
		}

}


