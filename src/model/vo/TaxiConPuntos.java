package model.vo;

public class TaxiConPuntos implements Comparable<TaxiConPuntos>{
	private double puntos;
	private String id;
	private String compania;

	public TaxiConPuntos(String id, String compania, double puntos) {
		this.setCompania(compania);
		this.setId(id);
		this.puntos=puntos;
	}

	/**
	 * @return puntos - puntos de un Taxi
	 */
	public double getPuntos(){
		return puntos;
	}



	public String getCompania() {
		return compania;
	}

	public void setCompania(String compania) {
		this.compania = compania;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	@Override
	public int compareTo(TaxiConPuntos o) {


		if( puntos-o.getPuntos()>0)
			return 1;
		else if(puntos==o.getPuntos())
			return 0;
		else
			return -1;
	}
}
