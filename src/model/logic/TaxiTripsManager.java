package model.logic;

import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.stream.JsonReader;

import API.ITaxiTripsManager;
import model.data_structures.IList;
import model.data_structures.Lista;
import model.data_structures.ListaDoblementeEncadenada;
import model.vo.ClassManager;
import model.vo.ServiceVo;
import model.vo.Servicio;
import model.vo.Taxi;
import model.vo.TaxiConPuntos;
import model.vo.TaxiConServicios;
import model.vo.TaxiEnZona;

public class TaxiTripsManager implements ITaxiTripsManager {
	public static final String DIRECCION_SMALL_JSON = "./data/taxi-trips-wrvz-psew-subset-small.json";
	public static final String DIRECCION_MEDIUM_JSON = "./data/taxi-trips-wrvz-psew-subset-medium.json";
	public static final String DIRECCION_LARGE_JSON = "./data/taxi-trips-wrvz-psew-subset-large";
	public static final String DIRECCION_02= "/taxi-trips-wrvz-psew-subset-02-02-2017.json";
	public static final String DIRECCION_03= "/taxi-trips-wrvz-psew-subset-03-02-2017.json";
	public static final String DIRECCION_04= "/taxi-trips-wrvz-psew-subset-04-02-2017.json";
	public static final String DIRECCION_05= "/taxi-trips-wrvz-psew-subset-05-02-2017.json";
	public static final String DIRECCION_06= "/taxi-trips-wrvz-psew-subset-06-02-2017.json";
	public static final String DIRECCION_07= "/taxi-trips-wrvz-psew-subset-07-02-2017.json";
	public static final String DIRECCION_08= "/taxi-trips-wrvz-psew-subset-08-02-2017.json";






	private ClassManager administrador;





	// //1C
	public boolean cargarSistema(String direccionJson) 
	{
		try
		{

			administrador=new ClassManager();
			if(direccionJson==DIRECCION_LARGE_JSON)
				cargarLarge();
			else
				cargarSmallMedium(direccionJson);
			administrador.acomodarPromedio();
			administrador.inicializarTablasListasArboles();
			administrador.R1C_OrdenarTaxisPorPuntos();
			administrador.inicializarArbolServRGFH();

			System.out.println("Se guardaron en la lista "+administrador.getCompanias().size()+ " Compa�ias en total");
			System.out.println("Se guardaron en la lista "+administrador.getTaxis().size()+ " Taxis en total");
			System.out.println("Se guardaron en la lista "+administrador.getServicios().size()+ " Servicios en total");
		}
		catch (Exception e) 
		{

			e.printStackTrace();;
		}


		return true;
	}


	public void cargarSmallMedium(String direccionJson) throws Exception
	{
		//Crear un InputStream como un new FileInputStream, el parametro que recibe es el link directorio del archivo json
		InputStream inputStream = new FileInputStream(direccionJson);

		
		//Inicializar un json reader que recibe como parametro el inputstream anteriormente creado

		JsonReader reader = new JsonReader(new InputStreamReader(inputStream, "UTF-8"));
		Gson gson = new GsonBuilder().create();

		// Esto recorre todos los elementos que estan en el json. Como no los guarda es mas memory friendly
		reader.beginArray();
		int contador=0;

		while (reader.hasNext())
		{
			// Instancia un servicio del json
			ServiceVo nServ = gson.fromJson(reader, ServiceVo.class);


			String fechaHoraInicial=nServ.getTrip_start_timestamp()==null?"":nServ.getTrip_start_timestamp();
			String fechaHoraFinal=nServ.getTrip_end_timestamp()==null?"":nServ.getTrip_end_timestamp();

			administrador.agregarServicio(1,nServ.getTrip_id()==null?"":nServ.getTrip_id(),
					nServ.getTaxi_id()==null?"":nServ.getTaxi_id(),
					nServ.getTrip_seconds()==null?"0":nServ.getTrip_seconds(),
					nServ.getTrip_miles()==null?"0":nServ.getTrip_miles(),
					nServ.getTrip_total()==null? "0":nServ.getTrip_total(),
					fechaHoraInicial.equals("")?"-1":fechaHoraInicial.substring(0,fechaHoraInicial.indexOf("T")),
					fechaHoraInicial.equals("")?"-1":fechaHoraInicial.substring(fechaHoraInicial.indexOf("T")+1),
					fechaHoraFinal.equals("")?"9999-99-99":fechaHoraFinal.substring(0,fechaHoraFinal.indexOf("T")),
					fechaHoraFinal.equals("")?"99:99:99.999":fechaHoraFinal.substring(fechaHoraFinal.indexOf("T")+1),
					nServ.getCompany()==null? "Independent Owner":nServ.getCompany(),
					nServ.getPickup_community_area()==null?"-1":nServ.getPickup_community_area(),
					nServ.getDropoff_community_area()==null?"-1":nServ.getDropoff_community_area(),
					nServ.getPickup_centroid_latitude(),
					nServ.getPickup_centroid_longitude());
			contador++;

		}

		System.out.println("Se cargaron "+contador+" servicios");

		reader.close();
	}


	public void cargarLarge() throws Exception
	{
		String direccion="";
		int contador=0;

		for(int i=1;i<8;i++)
		{
			if(i==1)
				direccion=DIRECCION_02;
			else if(i==2)
				direccion=DIRECCION_03;
			else if(i==3)
				direccion=DIRECCION_04;
			else if(i==4)
				direccion=DIRECCION_05;
			else if(i==5)
				direccion=DIRECCION_06;
			else if(i==6)
				direccion=DIRECCION_07;
			else
				direccion=DIRECCION_08;

			int contador1=0;

			//Crear un InputStream como un new FileInputStream, el parametro que recibe es el link directorio del archivo json
			InputStream inputStream = new FileInputStream(DIRECCION_LARGE_JSON+direccion);

			//Inicializar un json reader que recibe como parametro el inputstream anteriormente creado

			JsonReader reader = new JsonReader(new InputStreamReader(inputStream, "UTF-8"));
			Gson gson = new GsonBuilder().create();

			// Esto recorre todos los elementos que estan en el json. Como no los guarda es mas memory friendly
			reader.beginArray();
			while (reader.hasNext())
			{

				// Instancia un servicio del json
				ServiceVo nServ = gson.fromJson(reader, ServiceVo.class);


				String fechaHoraInicial=nServ.getTrip_start_timestamp()==null?"":nServ.getTrip_start_timestamp();
				String fechaHoraFinal=nServ.getTrip_end_timestamp()==null?"":nServ.getTrip_end_timestamp();

				administrador.agregarServicio(1,nServ.getTrip_id()==null?"":nServ.getTrip_id(),
						nServ.getTaxi_id()==null?"":nServ.getTaxi_id(),
						nServ.getTrip_seconds()==null?"0":nServ.getTrip_seconds(),
						nServ.getTrip_miles()==null?"0":nServ.getTrip_miles(),
						nServ.getTrip_total()==null? "0":nServ.getTrip_total(),
						fechaHoraInicial.equals("")?"-1":fechaHoraInicial.substring(0,fechaHoraInicial.indexOf("T")),
						fechaHoraInicial.equals("")?"-1":fechaHoraInicial.substring(fechaHoraInicial.indexOf("T")+1),
						fechaHoraFinal.equals("")?"9999-99-99":fechaHoraFinal.substring(0,fechaHoraFinal.indexOf("T")),
						fechaHoraFinal.equals("")?"99:99:99.999":fechaHoraFinal.substring(fechaHoraFinal.indexOf("T")+1),
						nServ.getCompany()==null? "Independent Owner":nServ.getCompany(),
						nServ.getPickup_community_area()==null?"-1":nServ.getPickup_community_area(),
						nServ.getDropoff_community_area()==null?"-1":nServ.getDropoff_community_area(),
						nServ.getPickup_centroid_latitude()==null?"null":nServ.getPickup_centroid_latitude(),
						nServ.getPickup_centroid_longitude()==null?"null":nServ.getPickup_centroid_longitude());

				contador++;
				contador1++;

			}
			System.out.println("Se cargaron "+contador1+" servicios");
			if(i>1)
				administrador.agregarServicio(0,null, null, null, null, null, null,null, null, null, null, null,null,null,null);


			reader.close();

		}

		System.out.println("Hay "+contador+ " servivios en total");
	}


	
	@Override
	public ListaDoblementeEncadenada<TaxiEnZona> A1TaxiConMasServiciosEnZonaParaCompania(int zonaInicio, String compania) {
		// TODO Auto-generated method stub
		return administrador.A1TaxiConMasServiciosEnZonaParaCompania(zonaInicio+"",compania);
	}


	@Override
	public ListaDoblementeEncadenada<Servicio> A2ServiciosPorDuracion(int duracion) {
		// TODO Auto-generated method stub
		return administrador.A2ServiciosPorDuracion(duracion);
	}


	@Override
	public ListaDoblementeEncadenada<Servicio> B1ServiciosPorDistancia(double distanciaMinima, double distanciaMaxima) {
		// TODO Auto-generated method stub
		return administrador.B1ServiciosPorDistancia(distanciaMinima, distanciaMaxima);
	}


	@Override
	public ListaDoblementeEncadenada<Servicio> B2ServiciosPorZonaRecogidaYLlegada(int zonaInicio, int zonaFinal, String fechaI, String fechaF, String horaI, String horaF) {
		// TODO Auto-generated method stub
		return administrador.B2ServiciosPorZonaRecogidaYLlegada(zonaInicio+"-"+zonaFinal, fechaI, fechaF,horaI,horaF);
	}


	@Override
	public TaxiConPuntos[] R1C_OrdenarTaxisPorPuntos() {
		// TODO Auto-generated method stub
		return administrador.R1C_OrdenarTaxisPorPuntos();
	}

	@Override
	public ListaDoblementeEncadenada<Servicio> R2C_LocalizacionesGeograficas(String taxiIDReq2C, double millasReq2C) {
		// TODO Auto-generated method stub
		return administrador.R2C_LocalizacionesGeograficas(taxiIDReq2C, millasReq2C) ;
	}

	@Override
	public ListaDoblementeEncadenada<Servicio> R3C_ServiciosEn15Minutos(String fecha, String hora,String zonaIni, String zonaFin) {
		// TODO Auto-generated method stub
		return administrador.R3C_ServiciosEn15Minutos(fecha, hora, zonaIni, zonaFin);
	}




}
